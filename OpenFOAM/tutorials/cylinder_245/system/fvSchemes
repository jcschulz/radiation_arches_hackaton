/*--------------------------------*- C++ -*----------------------------------*\
| =========                 |                                                 |
| \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox           |
|  \\    /   O peration     | Version:  2.2.0                                 |
|   \\  /    A nd           | Web:      www.OpenFOAM.org                      |
|    \\/     M anipulation  |                                                 |
\*---------------------------------------------------------------------------*/
FoamFile
{
    version     2.0;
    format      ascii;
    class       dictionary;
    location    "system";
    object      fvSchemes;
}
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

fluxScheme      Kurganov;

ddtSchemes
{
    default         Euler;
}

gradSchemes
{
    default         leastSquares;
}

divSchemes
{
    default         none;
    div(tauMC)      Gauss linear;
    div(phi,k)      Gauss upwind;
    div(phi,epsilon) Gauss upwind;
    div(elemental_diffusion_mass_flux) Gauss linear limited corrected 0.5;
    div(elemental_diffusion_energy) Gauss linear limited corrected 0.5;
    div(ddt(Ag))      Gauss linear limited corrected 0.5;
    div(Ag)      Gauss linear limited corrected 0.5;

}

laplacianSchemes
{
    default         Gauss harmonic corrected;
    laplacian(thermo:sigmaAngola,elecPotential) Gauss harmonic corrected;
    laplacian(Bi) Gauss linear corrected;
    laplacian(Ai) Gauss linear corrected;
    laplacian(Ag) Gauss linear corrected;
    laplacian(potential_gas) Gauss linear corrected;
    laplacian(muEff,U) Gauss harmonic corrected;
}

interpolationSchemes
{
    default         linear;
    reconstruct(rho_elementI) vanAlbada;
    reconstruct(rho) vanAlbada;
    reconstruct(U)  vanAlbadaV;
    reconstruct(T)  vanAlbada;
}

snGradSchemes
{
    default         corrected;
}


// ************************************************************************* //
