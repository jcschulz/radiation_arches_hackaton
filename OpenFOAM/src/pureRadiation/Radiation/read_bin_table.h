/**                                                                                                                                                                                                                                                                                                                         
 * @file read_bin_table.h                                                                                                                                                                                                                                                                                                   
 * @author Jeremie Meurisse (jeremie.meurisse@gmail.com)                                                                                                                                                                                                                                                                    
 * @date March 2017                                                                                                                                                                                                                                                                                                         
 * @brief Interpolate the opacity and the black body intensity from the bin table.                                                                                                                                                                                                                                 
 * @details                                                                                                                                                                                                                                                                                                                 
 * Compute the linear interpolation of the black body intensity and opacity for a given temperature and pressure.                                                                                                                                                                                                           
 * \n
 * Source file:
 *   - read_bin_table.C
 */

#ifndef __READ_BIN_TABLE_H_INCLUDED__
#define __READ_BIN_TABLE_H_INCLUDED__

#include<iostream>
#include<math.h>
#include<fstream>
#include<vector>
#include<cassert>
#include<algorithm>
#include "fvCFD.H"

class table_data_bin
{
  
 private:
  /// Number of bins, temperatures and pressures in the bin table.
  int nBins, nTemp, nPress;
  /// Current bin
  int bin;
  /// Black body intensities, opacities, pressures and temperatures vector from the bin table.
   std::vector<double> Pressures, Temperatures,
    m_opacity, m_source;

 public:
   /**
    * @brief Read the bin table
    * @param table_ bin table.
    * @param nB number of bins
    * @param nP number of pressures
    * @param nT number of temperatures
    */
   void read_table(List<scalar>& table_, 
		   scalar& nB,
		   scalar& nP,
		   scalar& nT);

   /**
    * @brief Compute the linear interpolation of the black body intensity and the opacity.
    * @param[in] P pressure [Pa]
    * @param[in] T temperature [K]
    * @param[out] source black body intensity \f$[W/m^2]\f$
    * @param[out] opacity opacity \f$[m^{-1}\f$]
    */
   void calc_value(    List<scalar>& P,
			List<scalar>&  T,
			List<float>& source,
		       List<float>& opacity);  

  void setbin(int i) {bin = i;}

  int Binmax () {return nBins;}

};

#endif // __READ_BIN_TABLE_H_INCLUDED__
