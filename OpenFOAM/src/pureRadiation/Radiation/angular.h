/**                                                                                                                                                                                                                                                                                                                         
 * @file angular.h                                                                                                                                                                                                                                                                                                          
 * @author Jeremie Meurisse (jeremie.meurisse@gmail.com)                                                                                                                                                                                                                                                                    
 * @date March 2017                                                                                                                                                                                                                                                                                                         
 * @brief Angular integration in the radiation.                                                                                                                                                                                                                                                                 
 * @details
 *  The total radiative heat flux \f$ \vec{q}^{rad} \f$ \f$ [W m^{-2}] \f$ is given by integrating the spectral radiative heat flux \f$\vec{q}^{rad}_\lambda\f$ \f$[W m^{-2} \mu m^{-1}]\f$ \n                                                                                                                             
 * over the entire spectrum with wavelength \f$\lambda\f$ \f$[\mu m]\f$. We approximate the integrale by a sum of discrete spectral radiative heat flux \f$\vec{q}^{rad}_\nu\f$ \f$[W m^{-2}]\f$  with a number \f$N_b\f$ of bins.                                                                                          
 * \f[                                                                                                                                                                                                                                                                                                                      
 * \vec{q}^{rad} = \int^\infty_0 \vec{q}^{rad}_\lambda d \lambda \approx \sum^{N_b}_{\nu=1}  \vec{q}^{rad}_\nu                                                                                                                                                                                                              
 * ~~~~ with ~~ \vec{q}^{rad}_\nu \approx \int^{\lambda_{\nu+1}}_{\lambda_\nu}   \vec{q}^{rad}_\lambda d \lambda  ~~~~ \nu = 1.. N_b\f]                                                                                                                                                                                     
 * \n                                                                                                                                                                                                                                                                                                                       
 * \f$\vec{q}^{rad}_\nu\f$ is found by integrating the radiative intensity \f$ I_\nu\f$  \f$[W m^{-2} sr^{-1}]\f$ in direction \f$\vec{\Omega} \f$ over the complete solid angle \f$\omega\f$ \f$[sr]\f$. \n                                                                                                                
 * We discretize it by the Discrete Ordinates method to obtain a set of 1-D steady state radiative transfer equations (RTE) in discrete directions \f$\vec{\Omega}_d\f$. \n                                                                                                                                                 
 * The integral over the solid angle is replaced by a weighted vector sum of discrete radiative intensities \f$I^d_{\nu}\f$ \f$[W m^{-2}]\f$ in these discrete directions \f$\vec{\Omega}_d \f$. \n                                                                                                                         
 * Gaussian type quadrature is employed to determine discrete directions \f$\vec{\Omega}_d\f$ and weights \f$a_d\f$ for different degrees of approximation (\f$N_d\f$ directions).                                                                                                                                          
 * \f[                                                                                                                                                                                                                                                                                                                      
 * \vec{q}^{rad}_\nu  = \int_{4\pi} \vec{\Omega} I_\nu(\vec{\Omega})  d \vec{\Omega} \approx \sum^{N_d}_{d=1} a_d I^d_{\nu} \vec{\Omega}_d                                                                                                                                                                                  
 * \f]
 * \n \n                                                                                                                                                                                                                                                                                                                    
 * This class will give \f$\vec{\Omega}_d\f$ in function of \f$N_d\f$.                                                                                                                                                                                                                                                      
 * \n
 * Source file:
 *   - angular.C
 */

#ifndef __ANGULAR_H_INCLUDED__
#define __ANGULAR_H_INCLUDED__

#include<iostream>
#include<cmath>
#include<vector>

# define PI           3.14159265358979323846  /* pi */

/// Angular integration for the radiation
class angular
{
 public:
  /// Discrete weights \f$a_d\f$
  std::vector<double> m_weight;
  /// Discrete direction \f$\Omega_d\f$
  std::vector<std::vector<double> > m_dirs;
  /// Number of directions \f$N_d\f$
  int nDirs;
  /// Number of direction types
  int m_nDirTypes;
  
 public:
  /// Initialize the angular integration for the radiation by giving the discrete directions \f$\Omega_d\f$
  void initialize(int, int, int);

};

#endif // __ANGULAR_H_INCLUDED__
