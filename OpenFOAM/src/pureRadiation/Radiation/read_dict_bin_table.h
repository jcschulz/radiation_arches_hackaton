/**                                                                                                                                                                                                                                                                                                                         
 * @file read_dict_bin_table.h                                                                                                                                                                                                                                                                                            
 * @author Jeremie Meurisse (jeremie.meurisse@gmail.com)                                                                                                                                                                                                                                                                    
 * @date March 2017                                                                                                                                                                                                                                                                                                         
 * @brief Read the bin table.                                                                                                                                                                                                                                                                                      
 * @details                                                                                                                                                                                                                                                                                                                 
 * Read the bin dictionary from "system/airBinDict" folder and then fill the bin table. \n
 * The dictionary has the values of the black body intensity and the opacity for different pressures (1 and 10 atm) and temperatures (300 to 15000 K). \n
 * We divided the entire spectrum in 100 bins. \n
 * For each pressure, temperature and bin, the table gives:
 * - a mean value of the black body intensity \f$\overline{S_{\lambda}} |_{\nu} ~(p,T) \f$ \f$[W cm^{-2}]\f$ \n 
 * - a mean value of the opacity times the black body intensity \f$ \overline{\kappa_{\lambda} S_{\lambda}} |_\nu ~(p,T)  \f$ \f$[W cm^{-3}]\f$. \n
 * \f[
 * \overline{S_{\lambda}}|_\nu ~(p,T) \approx \int^{\lambda_{\nu+1}}_{\lambda_\nu}   S_{\lambda}(p,T)  ~  d \lambda 
 * ~~~~ with ~~ \nu = 1.. N_b    
 * \f]
 * \f[
 *  \overline{\kappa_{\lambda} S_{\lambda}}|_\nu ~(p,T) \approx \int^{\lambda_{\nu+1}}_{\lambda_\nu}  \kappa_\lambda(p,T)  ~ S_{\lambda}(p,T) ~  d \lambda 
 * ~~~~ with ~~ \nu = 1.. N_b                                                                                                                                                                                                                                                                                              
 * \f]
 * \n
 * The mean opacity \f$\overline{\kappa_\lambda}|_{\nu} ~(p,T)\f$ \f$[cm^{-1}]\f$ can be estimated with these two previous values.       
 * \f[
 *  \overline{\kappa_{\lambda}}|_\nu~(p,T)= \frac{ \overline{\kappa_{\lambda} S_{\lambda}}|_\nu ~ (p,T)}{\overline{S_{\lambda}}|_\nu~(p,T)}
 * ~~~~ with ~~ \nu = 1.. N_b
 * \f]
 * \f$N_b\f$ = number of bins
 */

// Read the bin table 
const word dictName("airBinDict");

word regionName;
word regionPath;

// Check if the region is specified otherwise mesh the default region                                       
if (args.optionReadIfPresent("region", regionName, polyMesh::defaultRegion))
  {
    Info<< nl << "Generating mesh for region " << regionName << endl;
    regionPath = regionName;
  }

fileName dictPath;

// Check if the dictionary is specified on the command-line                                                 
if (args.optionFound("dict"))
  {
    dictPath = args["dict"];

        dictPath =
	  (
	   isDir(dictPath)
          ? dictPath/dictName
	   : dictPath
	   );
  }
// Check if dictionary is present in the constant directory                                                 
 else if
   (
        exists
        (
	 runTime.path()/runTime.constant()
	 /regionPath/polyMesh::meshSubDir/dictName
	 )
    )
   {
        dictPath =
	  runTime.constant()
	  /regionPath/polyMesh::meshSubDir/dictName;
   }
// Otherwise assume the dictionary is present in the system directory                                       
 else
   {
     dictPath = runTime.system()/regionPath/dictName;
   }

IOdictionary airBinDictIO
(
  IOobject 
  (
   dictPath,
   runTime,
   IOobject::MUST_READ,
   IOobject::NO_WRITE,
        false
   )
 );

if (!airBinDictIO.headerOk())
  {
    FatalErrorIn(args.executable())
            << "Cannot open mesh description file\n    "
            << airBinDictIO.objectPath()
            << nl
            << exit(FatalError);
  }

Info<< "--- Reading air-bin table ---" <<endl;
dictionary info = airBinDictIO.subDict("info");

scalar nBins(readScalar(airBinDictIO.subDict("info").lookup("nBins")));
scalar nPress(readScalar(airBinDictIO.subDict("info").lookup("nPress")));
scalar nTemp(readScalar(airBinDictIO.subDict("info").lookup("nTemp")));

List<scalar> table_
(
  airBinDictIO.lookup("table")
);
                                                          
