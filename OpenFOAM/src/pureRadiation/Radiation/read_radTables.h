/**                                                                                                                                                                                                                                                                                                                         
 * @file read_radTables.h                                                                                                                                                                                                                                                                                              
 * @author Jeremie Meurisse (jeremie.meurisse@gmail.com)                                                                                                                                                                                                                                                                    
 * @date March 2017                                                                                                                                                                                                                                                                                                         
 * @brief Read the radiation tables.                                                                                                                                                                                                                                                                                       
 * @details          
 * Read the radiation tables from "constant/radTables" folder:
 * - Normal to the faces of every cell surfaces
 * - Neighbors ID of every cells
 * - Advance order for the spectral integration
 * - Volume of every cells
 */

// Read the radiation tables
Info << "--- Reading radiation tables ---" <<endl;
Info << "[0%] reading normal faces table " << endl;
// Normal to the faces of every cells: size = n_cells x number_sides 
List<List<vector> > normal_faces_(mesh_info.lookup("normal_faces"));
normal_faces=normal_faces_;

// Neighbors of every cells: size = n_cells x number_sides
Info <<"[25%] reading elem neighbor table " << endl;
List<List<int> > elem_neighbor_(mesh_info.lookup("neihgbors"));
elem_neighbor=elem_neighbor_;

// Advance order for the integration: size = n_cells x n_direction
Info <<"[50%] reading advance order table " << endl;
List<List<int> >  advance_order_(mesh_info.lookup("advance_order"));
advance_order=advance_order_;

// Volume of every cells: size = n_cells
Info <<"[75%] reading volume table " << endl;
List<float> elem_volume_table_(mesh_info.lookup("elem_volume"));
elem_volume_table=elem_volume_table_;

n_elem_table =readScalar(mesh_info.lookup("mesh_size")); 
n_Dir_table=readScalar(mesh_info.lookup("number_directions"));
number_sides_table = readScalar(mesh_info.lookup("number_sides"));
Info <<"[100%] Done reading radiation tables " << endl;

// Verify the radiation tables 
if(nDir==(int)n_Dir_table)
  {}
else
  {
   Info << "\nError with the number of directions !\nYou should change constant/radTables \n"<< endl;
  return 0;
  }
if((number_sides==(int)number_sides_table))
  {}
 else
    {
    Info << "\nError with the number of sides !\nYou should change constant/radTables \n"<< endl;
    return 0;
  }

// Print information
Info << "Mesh information: number cells = " << n_elem << ", number of directions = " << n_Dir_table << endl;
Info << "Read radiation tables:" << endl;
Info << "1) advance_order[n_cells][nDir] size = " << advance_order.size() << "x" << advance_order[0].size()<<endl;
Info <<  "2) normal_faces[n_cells][number_sides] size = " << normal_faces.size() << "x" << normal_faces[0].size()<<endl;
Info << "3) elem_neighbor[n_cells][number_sides] size = " << elem_neighbor.size() << "x" << elem_neighbor[0].size()  << endl;
Info << "4) elem_volume[n_cells] size = " << elem_volume_table.size()  << endl;
Info << endl;
 
