/**                                                                                                                                                                                                                                                                                                                         
 * @file global_pT.h                                                                                                                                                                                                                                                                                                   
 * @author Jeremie Meurisse (jeremie.meurisse@gmail.com)                                                                                                                                                                                                                                                                    
 * @date March 2017                                                                                                                                                                                                                                                                                                         
 * @brief Global cell index for p,T (multiprocessing)                                                                                                                                                                                                                                  
 * @details Give the global cell index of the pressure and temperature in case of multiprocessing. \n  
 * Each processor has the radiation tables for the entire mesh   
 */
// Global cell index of the pressure and temperature
List<scalar> p_global(n_elem);
List<scalar> T_global(n_elem);

for(int i = 0 ;i < n_elem ; i++)
  {
    p_global[i]=0;
    T_global[i]=0;

  }

if (Pstream::parRun())
  {
    word path=word("constant/polyMesh/cellProcAddressing");

    labelIOList localCellProcAddr
      (
           IOobject
           (
            path,
            mesh,
            IOobject::MUST_READ,
            IOobject::NO_WRITE
            )
       );
 
    for(int i = 0 ; i < localCellProcAddr.size(); i++)
      {
	p_global[localCellProcAddr[i]]=p[i];
	T_global[localCellProcAddr[i]]=T[i];   
      }                          

reduce(p_global, sumOp<List<scalar> >());
reduce(T_global, sumOp<List<scalar> >());

  }
 else
   {
for(int i = 0 ;i < n_elem ; i++)
       {
         p_global[i]=p[i];
         T_global[i]=T[i];
       }
   }
