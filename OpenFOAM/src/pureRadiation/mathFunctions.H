/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     |
    \\  /    A nd           | Copyright (C) 2011 OpenFOAM Foundation
     \\/     M anipulation  |
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM.  If not, see <http://www.gnu.org/licenses/>.

InNamespace
    Foam

Description
    Math functions (derivative, integration, interpolation)

Details
    - \b derivative: second order, finite difference
    - \b integration: first order, trapezoidal
    - \b interpolation: first order, linear

    \*---------------------------------------------------------------------------*/

#ifndef mathFunctionsFoam_H
#define mathFunctionsFoam_H

#include "fvCFD.H"

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

namespace Foam
{

  // * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

  //- Linear interpolation from a table
  inline scalar linearInterpolation(scalarList x, scalarList f_x, scalar x0)
  {
    if (x.size() != f_x.size()) {
      FatalErrorInFunction << "Linear interpolation failed." << exit(FatalError);
    }
    scalar f_x0;
    labelList visitOrder;
    sortedOrder(x, visitOrder);
    x = scalarList(x, visitOrder);
    f_x = scalarList(f_x, visitOrder);

    if (x0 < x[0]) {
      //    WarningInFunction << "Interpolation lower bound." << endl;
      f_x0 = f_x[0];
      return  f_x0;
    }
    if (x0 > x[x.size()-1]) {
      //      WarningInFunction << "Interpolation upper bound." << endl;
      f_x0 = f_x[x.size()-1];
      return  f_x0;
    }
    forAll(x, i) {
      if (x[i] ==x0) {
	f_x0 = f_x[i];
	return  f_x0;
      }
    }
    forAll(x, i) {
      if (i>0) {
	if (x0 > x[i-1] && x0 < x[i]) {
	  scalar f_x0 = (f_x[i] - f_x[i-1])/(x[i] - x[i-1])*(x0 - x[i-1]) + f_x[i-1];
	  return f_x0;
	}
      }
    }

    FatalErrorInFunction << "Linear interpolation failed." << exit(FatalError);
    return 0;
  }


  // * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

} // End namespace Foam

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

#endif

// ************************************************************************* //
