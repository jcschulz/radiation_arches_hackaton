IOdictionary radiationTablesIO
(
  IOobject
  (
   "constant/radTables",
   runTime,
   IOobject::MUST_READ,
   IOobject::NO_WRITE,
        false
   )
 );

dictionary info = radiationTablesIO.subDict("mesh_info");
                                        
      List<List<vector> > normal_faces_1 
      (                                                                                                      
         info.lookup("normal_faces")                                                                   
    );                                                                                                      

List<List<scalar> > elem_neighbor_1
(
 info.lookup("neihgbors")
 );

List<List<int> > advance_order_1
(
 info.lookup("advance_order")
 );

List<scalar> elem_volume_1
(
 info.lookup("elem_volume")
 );

scalar n_elem_1(readScalar(info.lookup("mesh_size"))); 
scalar n_Dir_1(readScalar(info.lookup("number_directions")));

Info << "Mesh information: size = " << n_elem_1 << ", number of directions = " << n_Dir_1 << endl;
Info << "Read radiation tables:" << endl;
Info << "1) advance_order[n_elem][nDir] size = " << advance_order_1.size() << "x" << advance_order_1[0].size()<<endl;
Info <<  "2) normal_faces[n_elem][number_sides] size = " << normal_faces_1.size() << "x" << normal_faces_1[0].size()<<endl;
Info << "3) elem_neighbor[n_elem][number_sides] size = " << elem_neighbor_1.size() << "x" << elem_neighbor_1[0].size()  << endl;
Info << "4) elem_volume[n_elem] size = " << elem_volume_1.size()  << endl;
