#ifndef __READ_H_INCLUDED__
#define __READ_H_INCLUDED__

#include<iostream>
#include<math.h>
#include<fstream>
#include<vector>
#include<cassert>
#include<algorithm>
#include "fvCFD.H"

class table_data
{
  
 private:
  int nBins, nTemp, nPress;

  int bin;

   std::vector<double> Pressures, Temperatures,
    m_opacity, m_source;

 public:
   void read_table(List<scalar>& table_, 
		   scalar& nB,
		   scalar& nP,
		   scalar& nT);

   void calc_value(    volScalarField& P,
			volScalarField& T,
			List<scalar>& source,
		       List<scalar>& opacity);  

  void setbin(int i) {bin = i;}

  int Binmax () {return nBins;}

};

#endif // __READ_H_INCLUDED__
