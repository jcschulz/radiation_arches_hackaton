#ifndef __ANGULAR_H_INCLUDED__
#define __ANGULAR_H_INCLUDED__

#include<iostream>
#include<cmath>
#include<vector>

# define PI           3.14159265358979323846  /* pi */

class angular
{
 public:
  
  std::vector<double> m_weight;
  std::vector<double> ass;
  std::vector<std::vector<double> > m_dirs; 
  int nDirs;
  int m_nDirTypes;
  
 public:
  void initialize(int, int, int);

};

#endif // __ANGULAR_H_INCLUDED__
