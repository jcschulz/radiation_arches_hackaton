// ----- STEP 2 : COMPUTE THE ADVANCE ORDER ----
    
// Create the array advance_order, which contains, for each direction (2nd index), the list of elements to be advanced.
// The list is divided into "stages" that consist of a set of elements that can be advanced in parallel.
// These sets are terminated by a negative entry.
// E.g., if there are 8 elements and advance_order(1:8,1) = (1,2,-5,3,4,-8,6,-7), then elements (1,2,5) can be done first, and are in fact the boundary elements for direction 1,
// then elements (3,4,8) can be done; finally elements (6,7) can be done to complete the sweep in direction 1.
    
double order_count=0;
   
List<int> sdone; // done status of a element in a given direction at the end of a stage
List<int> cdone;  // done status of a element including those done in the current stage
sdone.resize(n_elem);
cdone.resize(n_elem);
    
List<List<int> > advance_order; // advance order for each directions
advance_order.resize(n_elem);
for(int e = 0 ; e < n_elem ; e++)
  {
    advance_order[e].resize(nDir);
  }
int m_last[nDir]; // last element in advance_order for each directions
    
     
for (int d = 0 ; d < nDir ; d++)
  {
    m_last[d]=0;

  }
int root=0;
int mesh_rank=0;
if(mesh_rank==root)
  {
    int dir_min=0;
    int dir_max=nDir;
  // Initialize element order index
    int m = 0;
        
        
    for(int d=dir_min; d<dir_max; d++)  // Loop for directions
      {
            
	std::cout << " ---- " << " direction " << d << " ----- " << std::endl;
	m_last[d]=0;
            
	// discrete directions of the Gaussian type quadrature for Radiaitive heat flux approximation
	double nx = angles.m_dirs[d][0];
	double ny = angles.m_dirs[d][1];
	double nz = angles.m_dirs[d][2];
	vector normal_direction (nx,ny,nz);
	
	//	Info << "normal_direction " << normal_direction << endl;
	for (int i = 0; i< n_elem ; i++)
	  {
	    sdone[i]=0;
	    cdone[i]=0;
	  }
            
	m = 0;
	
	while(m<n_elem)   // Loop m element
	  {
                
	    m_last[d] = m; // last element
                

	    for ( int n_el=n_elem-1; n_el >= 0 ; --n_el)  // Loop n_el element
	      {
                    
		// Store a pointer to the current element
		int elem_id = n_el;
                    
		// Flag to continue in Loop n_el element
		int flag = 0;
		double condition_1=0; // first condition to continue in Loop n_el element
		int condition_2=0; // second condition to continue in Loop n_el element
		
                    
		if(sdone[elem_id]==0) // the element is not done in previous stages
		  {
		    for(int ns=0; ns<number_sides;ns++) // Loop ns sides (faces)
		      {
		                                   
			// outgoing normal for the face ns of element n_el
			
			condition_1 = normal_faces[elem_id][ns]&normal_direction;
				                                
			    int neighbor_id = elem_neighbor[elem_id][ns]; // ID of the neighbor
			    
			    if (neighbor_id>=0) // If >=0, there is a neighbor for face ns of element n_el   
                            {
			        
			      condition_2= sdone[ neighbor_id ]  ;
			    }
			    else // the current face does not have neighbor
			      {
				condition_2=1;
			      }

			    // If the scalar product between the direction and the outgoing normal of side ns is negative, then element n_el cannot be advanced in this stage
			    if( condition_1  < 0.0 )
			      {
				if(condition_2== 0){               
				  flag=1;
				  break;
				  // goto cell_loop;
				}                            
			      }
 
			
                            
                            
                            
		      } // END Loop ns sides
                        
		    
		    if (flag==1)
		      {
			continue;
                            
		      }
			
                        
		    // Fill advance_order in the current stage
		    m=m+1;
		    advance_order[m-1][d]= elem_id;
		    cdone[elem_id]= 1;
		    if (d==23)
		      {
			order[advance_order[m-1][d]]=order_count;
		      }
		  } // END IF sdone[elem_id]==false
                    
	      }  // END loop n_el element
                
                
                
	    if (m==m_last[d]) // no element added to the list
	      {

		std::cout << " Error: no element added to the list " << std::endl;
		break;
                    
	      }
	    // fill advance_order with the last element of the stage
            if(d==23){
              order_count++;
	    }
	    advance_order[m-1][d]= - advance_order[m-1][d];
	    for (int i = 0; i< n_elem ; i++)
	      {
		sdone[i]=cdone[i];
	      }
                
	  } // END Loop m elements
           

      } // END loop for directions
  }



