
/**                                                            
 * @file   pureRadiation.C                                       
 * @brief  compute 3D radiative heat transfer.               
 * @author Jeremie Meurisse (jeremie.meurisse@gmail.com)                                          
 * @date   May, 2019                                         
 *                                                                                   
 */

#include <iostream>     // std::cout
#include <fstream>      // std::ifstream
#include "read_bin_table.H" // Read radiation bin and opacity tables
#include "angular.H" // Initialize radiation angular integration

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

double dot(std::vector<double>& a, std::vector<double>& b){

    if (a.size()!=3 || b.size()!=3){
        std::cout << "a.size()!=3 || b.size()!=3" << std::endl;
        return 0;
    }

   return a[0]*b[0]+a[1]*b[1]+a[2]*b[2];
}

int main(int argc, char *argv[])
{
    std::cout << "Reading tables" << std::endl;
#include "tables_reader.H"
#include "init_rad.H" // Initialize the radiation solver

  // Compute the 3D radiative transfer
#include "calc_radiation.H"

    std::ofstream os_out("divQrad.out");

    os_out << div_flux_sol.size() << std::endl;
    for(int e = 0 ; e<n_elem; e++)
    {
      os_out << div_flux_sol[e] << " ";
    }
    os_out << std::endl;
    std::cout << "end" << std::endl;


  // * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

  return 0;
}

// ************************************************************************* //
