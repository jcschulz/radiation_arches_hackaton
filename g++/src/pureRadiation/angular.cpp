#include "angular.H"

// Initialize the angular integration for the radiation
void angular::initialize(int nDir, int dim,int rank)
{
    nDirs = nDir;
    
    m_nDirTypes = 1.;
    
    m_weight.resize(nDirs);
    m_dirs.resize(nDirs);
    for(int n=0; n<nDirs; ++n)
    m_dirs[n].resize(dim);
    
    switch(nDirs)
    {
            case 8:
	      m_weight[0] = 4.*PI/static_cast<double>(nDirs);
            m_dirs[0][0] = 1./std::sqrt(3.);
            m_dirs[0][1] = 1./std::sqrt(3.);
            m_dirs[0][2] = 1./std::sqrt(3.);
            break;
            
            case 24:
	      m_weight[0] = 4.*PI/static_cast<double>(nDirs);
            m_dirs[0][0] = 0.2958759;
            m_dirs[0][1] = 0.2958759;
            m_dirs[0][2] = 0.9082483;
            break;
            
            case 48:
            m_weight[0] = 0.1609517;
            m_dirs[0][0] = 0.1838670;
            m_dirs[0][1] = 0.1838670;
            m_dirs[0][2] = 0.9656013;
            m_weight[1] = 0.3626469;
            m_dirs[1][0] = 0.1838670;
            m_dirs[1][1] = 0.6950514;
            m_dirs[1][2] = 0.6950514;
            break;
            
            case 80:
            m_weight[0] = 0.1712359;
            m_dirs[0][0] = 0.1422555;
            m_dirs[0][1] = 0.1422555;
            m_dirs[0][2] = 0.9795543;
            m_weight[1] = 0.0992284;
            m_dirs[1][0] = 0.1422555;
            m_dirs[1][1] = 1./std::sqrt(3.);
            m_dirs[1][2] = 0.8040087;
            m_weight[2] = 0.4617179;
            m_dirs[2][0] = 1./std::sqrt(3.);
            m_dirs[2][1] = 1./std::sqrt(3.);
            m_dirs[2][2] = 1./std::sqrt(3.);
            break;
            
        default:     // nDirs = 8
	  m_weight[0] = 4.*PI/static_cast<double>(nDirs);
            m_dirs[0][0] = 1./std::sqrt(3.);
            m_dirs[0][1] = 1./std::sqrt(3.);
            m_dirs[0][2] = 1./std::sqrt(3.);
            break;
    }
    
    
    //Note that it has been changed, because the counters start at 0
    unsigned int d = m_nDirTypes - 1;
    
    for (int dirType = 0; dirType < m_nDirTypes; dirType++)
    {
        for (int p = 0; p < 3; p++)
        {
            //Note that it's different because the counter starts at 0
            unsigned int l = p;
            
            unsigned int m = (p+1) % 3;
            unsigned int n = (p+2) % 3;
            
            if (p == 0 || m_dirs[dirType][0] != m_dirs[dirType][1] || m_dirs[dirType][1] != m_dirs[dirType][2] || m_dirs[dirType][2] != m_dirs[dirType][0])
            {
                for (int i = 0; i <= 1; i++)
                for (int j = 0; j <= 1; j++)
                for (int k = 0; k <= 1; k++)
                if ( p+i+j+k != 0)
                {
                    //Note that this is different because the counters are different
                    d += 1;
                    m_weight[d] = m_weight[dirType];
                    m_dirs[d][0] = std::pow(-1.,i)*m_dirs[dirType][l];
                    m_dirs[d][1] = std::pow(-1.,j)*m_dirs[dirType][m];
                    m_dirs[d][2] = std::pow(-1.,k)*m_dirs[dirType][n];
                    
                    if(rank == 0)
                    {
                        std::cout<<"Case1::dirTypes = " << dirType <<"\n";
                        std::cout<< "l = " << l << " m = " << m << " n = " << n  <<"\n";
                        std::cout<< "d = " << d <<"\n";
                        std::cout<< "dirs[" << d <<"] = ("<<  m_dirs[d][0] <<", " << m_dirs[d][1]
                        <<", "<<m_dirs[d][2]<<")\n";
                    }
                }
            }
            
            
            if (m_dirs[dirType][0] != m_dirs[dirType][1] &&
                m_dirs[dirType][1] != m_dirs[dirType][2]
                && m_dirs[dirType][2] != m_dirs[dirType][0])
            {
                for (int i = 0; i <= 1; i++)
                for (int j = 0; j <= 1; j++)
                for (int k = 0; k <= 1; k++)
                {
                    //Note that this is different because the counters are different
                    d += 1;
                    m_weight[d] = m_weight[dirType];
                    m_dirs[d][0] = std::pow(-1.,i)*m_dirs[dirType][l];
                    m_dirs[d][1] = std::pow(-1.,j)*m_dirs[dirType][m];
                    m_dirs[d][2] = std::pow(-1.,k)*m_dirs[dirType][n];
                    if(rank == 0)
                    {
                        std::cout<<"Case2::dirTypes = " << dirType <<"\n";
                        std::cout<< "l = " << l << " m = " << m << " n = " << n  <<"\n";
                        std::cout<< "d = " << d <<"\n";
                        std::cout<< "dirs[" << d <<"] = ("<<  m_dirs[d][0] <<", " <<
                        m_dirs[d][1] <<", "<<m_dirs[d][2]<<")\n";
                    }
                }
            }
        }
    }
}
