
//#include "read_bin_table.H"

/**                                                                                                                                                                                                                                                                                                                         
 * @file read_bin_table.h                                                                                                                                                                                                                                                                                                   
 * @author Jeremie Meurisse (jeremie.meurisse@gmail.com)                                                                                                                                                                                                                                                                    
 * @date March 2017                                                                                                                                                                                                                                                                                                         
 * @brief Interpolate the opacity and the black body intensity from the bin table.                                                                                                                                                                                                                                 
 * @details                                                                                                                                                                                                                                                                                                                 
 * Compute the linear interpolation of the black body intensity and opacity for a given temperature and pressure.                                                                                                                                                                                                           
 * \n
 * Source file:
 *   - read_bin_table.C
 */

#ifndef __READ_BIN_TABLE_H_INCLUDED__
#define __READ_BIN_TABLE_H_INCLUDED__

#include<iostream>
#include<math.h>
#include<fstream>
#include<vector>
#include<cassert>
#include<algorithm>

class table_data_bin
{
  
 private:
  /// Number of bins, temperatures and pressures in the bin table.
  int nBins, nTemp, nPress;
  /// Current bin
  int bin;
  /// Black body intensities, opacities, pressures and temperatures vector from the bin table.
   std::vector<double> Pressures, Temperatures,
    m_opacity, m_source;

 public:
   /**
    * @brief Read the bin table
    * @param table_ bin table.
    * @param nB number of bins
    * @param nP number of pressures
    * @param nT number of temperatures
    */
   void read_table(std::vector<double>& table_,
           int& nB,
           int& nP,
           int& nT);

   /**
    * @brief Compute the linear interpolation of the black body intensity and the opacity.
    * @param[in] P pressure [Pa]
    * @param[in] T temperature [K]
    * @param[out] source black body intensity \f$[W/m^2]\f$
    * @param[out] opacity opacity \f$[m^{-1}\f$]
    */
   void calc_value(    std::vector<double>& P,
            std::vector<double>&  T,
            std::vector<double>& source,
               std::vector<double>& opacity);

  void setbin(int i) {bin = i;}

  int Binmax () {return nBins;}

};

#endif // __READ_BIN_TABLE_H_INCLUDED__
// Read opacity and source data from binary tables
// and initialize arrays
void table_data_bin::read_table(std::vector<double>& table_, int& nB, int& nP, int& nT)
{
  nBins=nB;
  nPress=nP;
  nTemp=nT;
  
  Pressures.resize(nPress);                                                                                    
  Temperatures.resize(nTemp);                                                                                  
                                                                                                               
  int index=0;                                                                                                      
  for (int p = 0 ; p < nPress ; p++ )                                                                          
    {                                                                                                          
      Pressures[p]=table_[index] ;  
      index++;
    }                                                                                                          
                                                                                                               
  for (int t = 0 ; t < nTemp ; t++ )                                                                           
    {        
      Temperatures[t]=table_[index] ;                                                                    
      index++;
    }                                                                                                          
                                                                                                             
  m_opacity.resize(nPress*nBins*nTemp);                                                                        
  m_source.resize(nPress*nBins*nTemp);                                                                         
  for (int n = 0 ; n < nBins*nPress*nTemp ; n++ )                                                         
    {                                                                                                          
                                                             
      m_opacity[n]= table_[index] ;
      index++;
      m_source[n]= table_[index] ; 
      index++;
    }           

  std::cout << "nBin " << nBins << " nTemp " << nTemp << " nPress " << nPress << std::endl;

}


// Compute values based on coordinates using linear interpolation
#pragma acc routine seq
void table_data_bin::calc_value(     std::vector<double>& P,
                     std::vector<double>& T,
                     std::vector<double>& source,
                     std::vector<double>& opacity)
{
  
  int n_cell=P.size();
  int n_cell_T = T.size();
  if(n_cell == n_cell_T){
//    source.resize(n_cell);
//    opacity.resize(n_cell);
  }
  else{
 //   std::cout << "Error during source,opacity calculation: n_cell T != n_cell p" << std::endl;
    
  }


  for(int cellI =0 ; cellI<n_cell ; cellI++)
    {
  
      double P0 = 101325;
      double pressure=P[cellI]/P0;
      double temp_cell =  T[cellI];  

      // Minimum p,T
      if( pressure < 1 ){
	pressure=1+1e-10;
      }
      if( temp_cell < 300 ){
	temp_cell=300+1e-10;
      }
 
    
      if      (pressure<Pressures.front()) pressure = Pressures.front();
      else if (pressure>Pressures.back())  pressure = Pressures.back();

      if      (temp_cell<Temperatures.front()) temp_cell = Temperatures.front();
      else if (temp_cell>Temperatures.back())  temp_cell = Temperatures.back();
       
      // lookup the requested value and return a linear interpolation
      std::vector<double>::iterator T_upper_it =
	std::lower_bound(Temperatures.begin(),
			 Temperatures.end(),
			 temp_cell);

      assert (*T_upper_it >= temp_cell);
      
      const unsigned int idT = std::distance (Temperatures.begin(), T_upper_it) - 1;

      assert (idT+1 < Temperatures.size());
      assert (Temperatures[idT]   <= temp_cell);
      assert (Temperatures[idT+1] >= temp_cell);


      // Check for pressure
      std::vector<double>::iterator P_upper_it =
	std::lower_bound(Pressures.begin(),
			 Pressures.end(),
			 pressure);
      
      const unsigned int idP = std::distance (Pressures.begin(), P_upper_it) - 1;
      
      assert (idP+1 < Pressures.size());
      assert (Pressures[idP]   <= pressure);
      assert (Pressures[idP+1] >= pressure);

            
      // linear interpolation between specified values
      double opacity_l, opacity_u;
      double source_l, source_u;
      
      double percent = (temp_cell - Temperatures[idT]) / (Temperatures[idT+1] - Temperatures[idT]);
	     
      // P_lower
      opacity_l = m_opacity[idT + bin*nTemp + idP*nBins*nTemp] + percent*(m_opacity[(idT+1) + bin*nTemp + idP*nBins*nTemp] - m_opacity[idT + bin*nTemp + idP*nBins*nTemp]);
      source_l  = m_source[idT + bin*nTemp + idP*nBins*nTemp]
	+ percent*(m_source[(idT+1) + bin*nTemp + idP*nBins*nTemp] - m_source[idT + bin*nTemp + idP*nBins*nTemp]);

      // P_upper
      opacity_u = m_opacity[idT + bin*nTemp + (idP+1)*nBins*nTemp]
	+ percent*(m_opacity[(idT+1) + bin*nTemp + (idP+1)*nBins*nTemp] - m_opacity[idT + bin*nTemp + (idP+1)*nBins*nTemp]);
      source_u  = m_source[idT + bin*nTemp + (idP+1)*nBins*nTemp]
	+ percent*(m_source[(idT+1) + bin*nTemp + (idP+1)*nBins*nTemp] - m_source[idT + bin*nTemp + (idP+1)*nBins*nTemp]);

     
      opacity[cellI] = opacity_l + (pressure - Pressures[idP])/(Pressures[idP+1]-Pressures[idP])*(opacity_u - opacity_l);
      source[cellI]  = source_l + (pressure - Pressures[idP])/(Pressures[idP+1]-Pressures[idP])*(source_u - source_l);    
      
      // Convert to SI units
      opacity[cellI] *=1.e2; // [1/m]
      source[cellI]  *= 1.e6; // [W/m^3] 
      
    }
}



