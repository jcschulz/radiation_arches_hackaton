/**
 * @file calc_radiation.h
 * @author Jeremie Meurisse (jeremie.meurisse@gmail.com)
 * @date March 2017
 * @brief Calculate the divergence of the radiation heat flux used in the energy equation.
 * @details
 * We compute first the discrete radiative intensities \f$I^d_\nu ~[W/m^2]\f$ in discrete direction \f$\Omega_d\f$, discrete bin \f$\nu\f$ and cell \f$c\f$.
 * \f[
I^d_{\nu,c}
 = S_{\nu,c} + \frac{
 \sum^{N_{k_i}}_{k_i=1} ~ \left( S_{\nu,c} - I^d_{\nu,k_i} \right) ~ exp \left( \kappa_\nu  \vec{\Omega_d}  \cdot  \vec{x} \right) ~  \vec{\Omega_d} ~ \cdot ~ \Delta  \vec{S_c}}{\sum^{N_{k_o}}_{k_o=1} ~  exp \left( \kappa_\nu  \vec{\Omega_d}  \cdot  \vec{x} \right) ~  \vec{\Omega_d} ~ \cdot ~ \Delta  \vec{S_c}}
\f]
 *  \f$\vec{\Omega}_d\f$ = discrete direction for the angular integration. \n
 * \f$ \vec{S_c} = \vec{n}_c S_c \f$ with the face surface \f$S_c\f$ and the outgoing cell face normal \f$\vec{n}_c\f$ for cell \f$c\f$. \n
 * \f$S_{\nu,c}\f$ = mean black body radiative intensity \f$[W/m^2]\f$ in the center of cell \f$c\f$ and discrete direction \f$\vec{\Omega}_d\f$. \n
 * We split into an incoming part (\f$k_i \in  \vec{\Omega_d} \cdot \Delta  \vec{S_c} < 0 \f$) and outgoing part (\f$k_o \in \vec{\Omega_d} \cdot \Delta  \vec{S_c} > 0\f$). \n
 * Using the upstream intensity for the incoming part, and the cell value for the outgoing. \n
 * If the incoming face is part of boundary \f$\Gamma\f$, we use the black body radiative intensity (source term) at the center cell.
 * \n \n
 * Then we compute the divergence of radiative heat flux \f$ \vec{\nabla} \cdot \vec{q}^{rad} ~ [W/m^3]\f$
 * \f[ \vec{\nabla} \cdot \vec{q}^{rad} = \sum ^{N_b}_{\nu=1}\sum ^{N_d}_{d=1}\sum ^{N_c}_{c=1}  ~ \frac{ a_d I^d_{\nu,c} \vec{\Omega_d} \cdot  \vec{\Delta S_c}}{V_c} \f]
 * \f$N_b\f$ = number of discrete bins. \n
 * \f$N_d\f$ = number of discrete directions. \n
 * \f$N_c\f$ = number of neighbor cells. \n
 * \f$V_c\f$ = volume of cell \f$c\f$. \n
 * \f$a_d\f$ = weight from angular integration.
 */

// Initialization solution tables
for (int n = 0 ; n< n_elem; n++)
{

  I_sol[n]=0;
  div_flux_sol[n]=0;
  I_done[n]=0;
}

std::cout << "Solving RADIATION" << std::endl;
// Sum solutions of all bins and directions
#pragma acc data copy(div_flux_sol) create(dir_dot_n_A[:6], normal_direction, angles)
for(int b=bin_min; b<=bin_max; b++)   // Loop for all bins
{
    std::cout << "--- bin = " << b << std::endl;
  data.setbin(b);

  data.calc_value(pressure, temperature, source, opacity);


#pragma acc parallel loop present(div_flux_sol, dir_dot_n_A[:6], normal_direction, angles)
  for(int d=dir_min; d<dir_max; d++) { // Loop for angular integration
//    std::cout << "    dir = " << d << std::endl;
    // discrete weight of the Gaussian type quadrature for Radiaitive heat flux approximation
    weight  = angles.m_weight[d];

    // disctete direction of the Gaussian type quadrature for Radiaitive heat flux approximation
    nx = angles.m_dirs[d][0];
    ny = angles.m_dirs[d][1];
    nz = angles.m_dirs[d][2];
    //std::vector<double> normal_direction(3);
    normal_direction[0]=nx;
    normal_direction[1]=ny;
    normal_direction[2]=nz;

    for (int n = 0 ; n< n_elem; n++) {
      I_done[n]=0;
      I_sol[n]=0;
    }

    for (int m = 0 ; m < n_elem; m++ ) {
      // ID of the current element
      int elem_id= abs(advance_order[m][d]);
      double source_v ;
      double opacity_v;
      if (opacity[elem_id]<=1e-120 ||  source[elem_id]<=1e-120) {
        opacity_v =1e-120;
        source_v =1e-120;
      } else {
        opacity_v =opacity[elem_id];
        source_v =source[elem_id] /opacity[elem_id] ;
      }
      // Volume of element m
      double elem_volume_ = elem_volume[elem_id];

      double dir_dot_n_A_pos=0; // Positive part of direction dot outgoing normal of element elem_id
      double dir_dot_n_A_neg=0; // Negative part of direction dot outgoing normal of element elem_id
      double In_dir_dot_n_A_neg=0; // Negative part of radiative intensity (bin b, dir d) times direction dot outgoing normal of element elem_id



      for(int ns=0; ns<n_sides; ns++) { // Loop ns sides (faces)
        dir_dot_n_A[ns]=dot(normal_faces[elem_id][ns],normal_direction);

        // Exponantial method considers only the source term constant in the element elem_id
        if(use_exp_meth==1) {
          if( dir_dot_n_A[ns] <0) { // Incoming face
            dir_dot_n_A_neg+= dir_dot_n_A[ns];


            if (elem_neighbor[elem_id][ns]>=0) { // Incoming face with neighbor
              int neighbor_id =elem_neighbor[elem_id][ns];
              if (I_done[neighbor_id]==0) {  // Problem if upstream intensity is not already done


                // Find the ingoing normal from side ns of element elem_id
                double max_same_face = 0;
                int ns_max =0;

                for(int ns_neighbor = 0 ; ns_neighbor < n_sides; ns_neighbor++ ) {

                  double same_face = - dot(normal_faces[neighbor_id][ns_neighbor],normal_faces[elem_id][ns]);
                  if (same_face>max_same_face) {
                    max_same_face=same_face;
                    ns_max = ns_neighbor;
                  }

                }

                double dir_dot_n_A_neighbor = dot(normal_faces[neighbor_id][ns_max],normal_direction);

//                std::cout << "Bad I_sol" << std::endl;

//                break;
              }


              In_dir_dot_n_A_neg+=I_sol[neighbor_id]*dir_dot_n_A[ns]; // Incoming internal face, use neighbor's outgoing radiative intensity

            } else {
              In_dir_dot_n_A_neg+=source_v*dir_dot_n_A[ns]  ; // Boundary element, assume that the neighbor's outgoing radiative intensity is the same as the source equilibrium value of the current element center
            }

          }

        } else { // Normal method considers the source and the opacity term constant in the element elem_id

          if(dir_dot_n_A[ns]==0) {
            continue;
          }
          if(dir_dot_n_A[ns]>0) { // Upstream face

            dir_dot_n_A_pos+=dir_dot_n_A[ns];
          } else if (elem_neighbor[elem_id][ns]>=0) { // Incoming face with neighbor
            int neighbor_id =elem_neighbor[elem_id][ns];
            if (I_done[neighbor_id]==0) { // Problem if upstream intensity is not already done


              // Find the ingoing normal from side ns of element elem_id
              double max_same_face = 0;
              int ns_max =0;

              for(int ns_neighbor = 0 ; ns_neighbor < n_sides; ns_neighbor++ ) {
//                  vector normal_face_neighbor_1 = normal_faces[neighbor_id][ns_neighbor];
                double same_face = - dot(normal_faces[neighbor_id][ns_neighbor],normal_faces[elem_id][ns]);
                if (same_face>max_same_face) {
                  max_same_face=same_face;
                  ns_max = ns_neighbor;
                }

              }
              double dir_dot_n_A_neighbor = dot(normal_faces[neighbor_id][ns_max],normal_direction);

//              std::cout << "Bad I_sol" << std::endl;

//             break;
            }

            In_dir_dot_n_A_neg+=I_sol[neighbor_id]*dir_dot_n_A[ns]; // Incoming internal face, use neighbor's outgoing radiative intensity

          } else { // Incoming face without neighbor

            In_dir_dot_n_A_neg+= source_v*dir_dot_n_A[ns]  ; // Boundary element, assume that the neighbor's outgoing radiative intensity is the same as the source equilibrium value of the current element center

          }


        }

      } // END Loop ns sides (faces)


      double Ic;

      // Calculate radiative intensity for bin b and direction d
      if(use_exp_meth==1) {
        I_done[elem_id]=1;
        double Lc = elem_volume_ /(-dir_dot_n_A_neg); // Average length accross the element in direcion d
        double half_exp = std::exp(-0.5*Lc*opacity_v);
        I_sol[elem_id]=(In_dir_dot_n_A_neg/dir_dot_n_A_neg)*pow(half_exp,2)+(1-pow(half_exp,2))*source_v; // Outgoing face intensity = intensity all the way across the element

        Ic = (In_dir_dot_n_A_neg/dir_dot_n_A_neg)*half_exp+(1-half_exp)*source_v; // Radiative intensity value halfway across, at the element center

      } else {
        I_done[elem_id]=1;
        Ic = (opacity_v*source_v*elem_volume_ - In_dir_dot_n_A_neg)/(opacity_v*elem_volume_ +dir_dot_n_A_pos) ;
        I_sol[elem_id]=Ic;
      }

      // Acculumate the outgoing part of the divergence of the radiative heat flux
      double In_dir_dot_n_A =In_dir_dot_n_A_neg;
      for(int nf=0; nf<n_sides; nf++) {
        if (dir_dot_n_A[nf]>0) { // Outgoing face: use the outgoing radiative intensity of this element
          In_dir_dot_n_A+=I_sol[elem_id]*dir_dot_n_A[nf];
        }
      }// END FOR FACES nf

      div_flux_sol[elem_id]+=In_dir_dot_n_A*weight;

    } // end loop element

  } // END Loop for angular integration

} // END Loop bins





for(int e = 0 ; e<n_elem; e++)
{
  div_flux_sol[e]/=elem_volume[e];
}

// Compute the checksum
double checksum=0;
for(int e = 0 ; e<n_elem; e++)
{
  checksum+=div_flux_sol[e];
}
std::cout << "checksum = " << checksum << std::endl;

